import { GeneService } from './services/gene.service';
import { SocketService } from './services/socket.service';
import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  constructor(
    private ss: SocketService,
    private gs: GeneService
  ) { }

  ngOnInit() {
    this.gs.genesBeingProcessed().subscribe(g => {
      if (g) {
        this.ss.pintronStatus = true;
      }
    });
  }

}
