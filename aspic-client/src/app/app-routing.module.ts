import { TranscriptomicGraphComponent } from './components/transcriptomic-graph/transcriptomic-graph.component';
import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { FastaInputComponent } from './components/fasta-input/fasta-input.component';
import { HomeComponent } from './components/home/home.component';


const routes: Routes = [
  {
    path: '',
    component: HomeComponent,
  },
  {
    path: 'fasta-input',
    component: FastaInputComponent
  },
  {
    path: 'transcriptomic-graph/:id',
    component: TranscriptomicGraphComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
