import { Gene } from './../models/gene';
import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';

const url = '/api/Genes';

@Injectable({
  providedIn: 'root'
})
export class GeneService {

  constructor(private http: HttpClient) { }

  getPintronTranscript(id: string): Observable<Gene> {
    return this.http.get<Gene>(`${url}/${id}`);
  }

  genesBeingProcessed(): Observable<Gene> {
    return this.http.get<Gene>(`${url}/ProcessingGene`);
  }

}
