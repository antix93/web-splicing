import { Gene } from './../models/gene';
import { Injectable } from '@angular/core';
import * as signalR from '@aspnet/signalr';
import { MatSnackBar } from '@angular/material';

@Injectable({
  providedIn: 'root'
})
export class SocketService {
  private pintronProcessing = false;

  get pintronStatus() {
    return this.pintronProcessing;
  }

  set pintronStatus(ps: boolean) {
    this.pintronProcessing = ps;
  }

  // For NGINX
  socketConnection = new signalR.HubConnectionBuilder().withUrl('/Socket').build();
  // For develop mode
  // socketConnection = new signalR.HubConnectionBuilder().withUrl('https://localhost:5001/Socket').build();

  constructor(
    private sb: MatSnackBar
  ) {
    this.socketConnection.start()
      .then(() => console.log('Start comunication with socket'))
      .catch(e => console.log('Error comunication with socket: ', e));

    // Listening to the server for finishing elaboration
    this.socketConnection.on('ElaborationResponse', (gene: Gene) => {
      this.pintronStatus = false;
      this.sb.open(`Elaboration endend for gene: ${gene.display_name}`, 'Ok', { duration: 8000 });
    });

    // Listening to the server for starting elaboration
    this.socketConnection.on('StartingElaboration', isStarted => {
      this.pintronStatus = isStarted;
    });

  }

  invokePintron(seq: string, g?: Gene) {
    this.pintronStatus = true;
    this.sb.open('Data elaboration started', 'Ok', { duration: 5000 });
    this.socketConnection.invoke('ElaborationForPintron', seq, g)
      .catch(e => {
        this.sb.open('Error during invocation', 'Ok', { duration: 5000 });
        this.pintronStatus = false;
        console.log('Error socket invocation: ', e);
      });
  }

}
