import { Injectable } from '@angular/core';
import { HttpHeaders, HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Xrefs } from '../models/xrefs';
import { Gene } from '../models/gene';
import { map } from 'rxjs/operators';
import { Sequence } from '../models/sequence';
import { Specie } from '../models/specie';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' }),
};

const headersFasta = {
  headers: new HttpHeaders({ 'Content-Type': 'text/x-fasta' }),
};

const ensembleUrl = 'https://rest.ensembl.org';

@Injectable({
  providedIn: 'root'
})
export class EnsemblService {

  constructor(private http: HttpClient) { }

  /* Looks up an external symbol and returns all Ensembl objects linked to it.
  This can be a display name for a gene/transcript/translation, a synonym or an externally linked reference.
  If a gene's transcript is linked to the supplied symbol the service will return both gene and transcript (it supports transient links). */
  getEnsambleId(species: Specie, gene: string): Observable<Xrefs[]> {
    return this.http.get<Xrefs[]>(`${ensembleUrl}/xrefs/symbol/${species.scientificName}/${gene}`, httpOptions);
  }

  /* Find the species and database for several identifiers.
  IDs that are not found are returned with no data. */
  postLookup(ensemblIds: { ids: string[] }): Observable<Gene[]> {
    return this.http.post<Gene[]>(`${ensembleUrl}/lookup/id`, ensemblIds, httpOptions).pipe(
      map(r => Object.keys(r).map(key => r[key]))
    );
  }

  /* Request multiple types of sequence by stable identifier.
  Supports feature masking and expand options. */
  getSequence(ensemblId: string): Observable<Sequence> {
    return this.http.get<Sequence>(`${ensembleUrl}/sequence/id/${ensemblId}`, httpOptions);
  }

  getFastaSequence(ensemblId: string): Observable<Blob> {
    return this.http.get(`${ensembleUrl}/sequence/id/${ensemblId}`,
      { headers: new HttpHeaders({ 'Content-Type': 'text/x-fasta' }), responseType: 'blob' });
  }
}
