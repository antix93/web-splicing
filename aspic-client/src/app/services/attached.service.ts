import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable } from 'rxjs';
import { Gene } from '../models/gene';

const url = '/api/Attached';

@Injectable({
  providedIn: 'root'
})
export class AttachedService {

  constructor(private http: HttpClient) { }

  uploadFasta(file: File, g?: Gene): Observable<boolean> {
    const formData = new FormData();
    formData.append('file', file);
    formData.append('g', JSON.stringify(g));

    return this.http.post<boolean>(`${url}`, formData);
  }
}
