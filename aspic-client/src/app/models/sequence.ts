export interface Sequence {
  id?: string;
  desc?: string;
  molecule?: string;
  query?: string;
  seq?: string;
  version?: number;
}
