export interface PintronOutput {
  genome?: Genome;
  introns?: Introns[];
  isoforms?: Isoforms[];
  number_of_predicted_isoforms?: number;
}

export interface Introns {
  BPS_score?: number;
  length?: number;
  pattern?: string;
  prefix?: string;
  suffix?: string;
  absolute_end?: number;
  absolute_start?: number;
  relative_end?: number;
  relative_start?: number;
}

export interface Isoforms {
  RefSeqID?: string;
  exons?: Exons[];
  introns?: number[];
}

export interface Exons {
  absolute_end?: number;
  absolute_start?: number;
  relative_end?: number;
  relative_start?: number;
}

interface Genome {
  length?: number;
  sequence_id?: string;
  strand?: string;
}
