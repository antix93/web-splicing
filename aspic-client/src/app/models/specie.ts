export interface Specie {
  id?: string;
  scientificName?: string;
  normalizedName?: string;
  commonName?: string;
}
