export interface Gene {
  id?: string;
  object_type?: string;
  source?: string;
  version?: number;
  seq_region_name?: string;
  species?: string;
  biotype?: string;
  db_type?: string;
  assembly_name?: string;
  description?: string;
  start?: number;
  strand?: number;
  display_name?: string;
  end?: number;
  logic_name?: string;
  elaboration?: boolean;
  pintron_output?: string;
}
