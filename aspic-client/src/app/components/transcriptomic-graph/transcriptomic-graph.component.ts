import { ShellService } from 'src/app/shell/shell.service';
import { PintronOutput, Exons } from './../../models/pintronOutput';
import { GeneService } from './../../services/gene.service';
import { switchMap, debounceTime, tap } from 'rxjs/operators';
import { Gene } from './../../models/gene';
import { Component, OnInit, ViewEncapsulation, ViewChild, ElementRef, HostListener } from '@angular/core';
import { ActivatedRoute, ParamMap, Router } from '@angular/router';
import * as d3 from 'd3';
import { Observable, fromEvent } from 'rxjs';

@Component({
  selector: 'app-transcriptomic-graph',
  templateUrl: './transcriptomic-graph.component.html',
  styleUrls: ['./transcriptomic-graph.component.css'],
  encapsulation: ViewEncapsulation.None
})
export class TranscriptomicGraphComponent implements OnInit {
  gene: Gene;
  json: PintronOutput;
  w: number;
  h: '100%';

  resizeObservable$: Observable<Event>;

  @ViewChild('box', { static: true }) containerBox: ElementRef;

  constructor(
    private route: ActivatedRoute,
    private r: Router,
    private gs: GeneService,
    private ss: ShellService
  ) { }

  gene$ = this.route.paramMap.pipe(
    switchMap((params: ParamMap) =>
      this.gs.getPintronTranscript(params.get('id')))
  );

  ngOnInit() {
    this.resizeObservable$ = fromEvent(window, 'resize');
    this.resizeObservable$.pipe(debounceTime(800)).subscribe(evt => {
      d3.select('svg').remove();
      this.drawChart(this.ss.sid);
    });

    this.gene$.subscribe(g => {
      if (g) {
        this.gene = g;
        this.json = JSON.parse(g.pintron_output);
        this.drawChart(this.ss.sid);
      } else {
        this.r.navigate(['/fasta-input']);
      }
    });
  }

  drawChart(sideNavClose: boolean) {
    this.w = this.containerBox.nativeElement.getBoundingClientRect().width - 100;
    const isoVis = d3.select('#iso-vis')
      .append('svg')
      .attr('width', this.w)
      .attr('height', this.h)
      .attr('id', 't')
      .attr('class', 'iso-box')
      .style('padding', '10px');

    const div = d3.select('#iso-vis').append('div')
      .attr('class', 'tooltip')
      .style('opacity', 0);

    let xScale;
    if (this.json.genome.strand === '-') {
      xScale = d3.scaleLinear()
        .domain([this.gene.end, this.gene.start])
        .range([110, this.w - 11]);
    } else {
      xScale = d3.scaleLinear()
        .domain([this.gene.start, this.gene.end])
        .range([110, this.w - 11]);
    }

    const axis = d3.axisBottom(xScale)
      .ticks(5);

    isoVis.append('g').call(axis);

    const intronData = Object.values(this.json.introns);
    const isoData = Object.values(this.json.isoforms);

    const exonsArray: Exons[] = [];
    for (let i = 1; i < this.json.number_of_predicted_isoforms + 1; i++) {
      for (let k = 0; k < this.json.isoforms[i].exons.length; k++) {
        exonsArray.push(this.json.isoforms[i].exons[k]);
      }
    }

    exonsArray.sort((a, b) => a['relative_start'] - b['relative_start'] || b['relative_end'] - a['relative_end']);

    let y = 50;
    const isoform = isoVis.append('g')
      .selectAll('g')
      .data(isoData)
      .enter().append('g')
      .attr('class', 'isoform')
      .each(function(d) {

        isoVis.attr('height', y + 50);

        const g = d3.select(this);

        if (d.RefSeqID !== undefined) {
          g.append('text')
            .text(d.RefSeqID)
            .attr('y', y);
        }

        g.append('g')
          .attr('class', 'exons')
          .selectAll('line')
          .data(d.exons)
          .enter().append('line')
          .attr('stroke', '#2B4C9C')
          .attr('stroke-width', 35)
          .each(function(e) {
            const gLine = d3.select(this);
            gLine.attr('x1', xScale(e.absolute_start))
              .attr('x2', xScale(e.absolute_end))
              .attr('y1', y)
              .attr('y2', y)
              .on('click', () => {
                div.transition()
                  .duration(200)
                  .style('opacity', .9);
                div.html('<b> 3UTR_length: </b> ' + e['3UTR_length']
                  + '<br><b>5UTR_length:</b> ' + e['5UTR_length']
                  + '<br><b>Chr start:</b> ' + e.absolute_start
                  + '<br><b>Chr end:</b> ' + e.absolute_end)
                  .style('left', () => {
                    if (sideNavClose) {
                      return (this.getBoundingClientRect().left - 80 + this.getBoundingClientRect().width / 2) + 'px';
                    } else {
                      return (this.getBoundingClientRect().left - 335 + this.getBoundingClientRect().width / 2) + 'px';
                    }
                  })
                  .style('top', (this.getBoundingClientRect().top - 147) + 'px');
              })
              .on('mouseout', () => {
                div.transition()
                  .duration(500)
                  .style('opacity', 0);
              });
          });

        g.append('g')
          .attr('class', 'introns')
          .selectAll('line')
          .data(d.introns)
          .enter().append('line')
          .attr('stroke', 'black')
          .attr('stroke-width', 5)
          .each(function(i) {
            const bLine = d3.select(this);
            bLine.attr('x1', xScale(intronData[i - 1].absolute_start))
              .attr('x2', xScale(intronData[i - 1].absolute_end))
              .attr('y1', y)
              .attr('y2', y)
              .on('click', () => {
                div.transition()
                  .duration(200)
                  .style('opacity', .9);
                div.html('<b> BPS score: </b> ' + intronData[i - 1].BPS_score
                  + '<br><b>Chr start:</b> ' + intronData[i - 1].absolute_start
                  + '<br><b>Chr end:</b> ' + intronData[i - 1].absolute_end
                  + '<br><b>Length:</b> ' + intronData[i - 1].length
                  + '<br><b>Pattern:</b> ' + intronData[i - 1].pattern
                  + '<br><b>Prefix:</b> ' + intronData[i - 1].prefix
                  + '<br><b>Suffix:</b> ' + intronData[i - 1].suffix)
                  .style('left', () => {
                    if (sideNavClose) {
                      return (this.getBoundingClientRect().left - 90 + this.getBoundingClientRect().width / 2) + 'px';
                    } else {
                      return (this.getBoundingClientRect().left - 345 + this.getBoundingClientRect().width / 2) + 'px';
                    }
                  })
                  .style('top', (this.getBoundingClientRect().top - 58) + 'px');
              })
              .on('mouseout', () => {
                div.transition()
                  .duration(500)
                  .style('opacity', 0);
              });

          });
        y = y + 50;
      });
  }

}
