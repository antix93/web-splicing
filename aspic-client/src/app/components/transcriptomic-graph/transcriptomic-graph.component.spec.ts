import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TranscriptomicGraphComponent } from './transcriptomic-graph.component';

describe('TranscriptomicGraphComponent', () => {
  let component: TranscriptomicGraphComponent;
  let fixture: ComponentFixture<TranscriptomicGraphComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TranscriptomicGraphComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TranscriptomicGraphComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
