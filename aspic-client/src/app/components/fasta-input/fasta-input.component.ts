import { SocketService } from './../../services/socket.service';
import { AttachedService } from './../../services/attached.service';
import { Component, OnInit, ViewChild, ElementRef, OnDestroy } from '@angular/core';
import { ShellService, ShellData } from 'src/app/shell/shell.service';
import { FormControl } from '@angular/forms';
import { Observable } from 'rxjs';
import { startWith, map, tap } from 'rxjs/operators';
import { EnsemblService } from 'src/app/services/ensembl.service';
import { Gene } from 'src/app/models/gene';
import { LoadingScreenService } from 'src/app/services/loading-screen';
import { MatSnackBar } from '@angular/material';
import { SpeciesService } from 'src/app/services/species.service';
import { Specie } from 'src/app/models/specie';

@Component({
  selector: 'app-fasta-input',
  templateUrl: './fasta-input.component.html',
  styleUrls: ['./fasta-input.component.css']
})
export class FastaInputComponent implements OnInit, OnDestroy, ShellData {
  @ViewChild('actions', { static: true }) actions: ElementRef;
  @ViewChild('title', { static: true }) title: ElementRef;
  @ViewChild('fasta', { static: false }) fastaFile;

  species: Specie[];
  speciesControl = new FormControl();
  filteredSpecies: Observable<Specie[]>;

  gene: string;
  genesInformation: Gene[] = [];

  get disableSearch() {
    if (!this.speciesControl.value || !this.gene) {
      return true;
    } else {
      return false;
    }
  }

  constructor(
    private ss: ShellService,
    private es: EnsemblService,
    private sps: SpeciesService,
    private as: AttachedService,
    private ls: LoadingScreenService,
    public socketService: SocketService,
    private sb: MatSnackBar
  ) { }

  ngOnInit() {
    this.ss.register(this);

    this.sps.getSpecies().subscribe(s => {
      this.species = s;

      const defaultSpecies = s.find(d => d.scientificName === 'Homo_sapiens');
      this.speciesControl.setValue(defaultSpecies);

      this.filteredSpecies = this.speciesControl.valueChanges
        .pipe(
          startWith<string | Specie>(''),
          map(value => typeof value === 'string' ? value : value.normalizedName),
          map(name => name ? this._filter(name) : this.species.slice())
        );
    });

  }

  ensemblId() {
    this.ls.startLoading();
    const selectedSpecie: Specie = this.speciesControl.value;
    this.es.getEnsambleId(selectedSpecie, this.gene).pipe(
      map(xref => xref.map(x => x.id))
    ).subscribe({

      next: r => {
        this.es.postLookup({ ids: r }).subscribe(g => {
          this.genesInformation = g.filter(_g => !_g.id.includes('LRG'));
          this.ls.stopLoading();
          if (!g.length) {
            this.sb.open('No result', 'Ok', { duration: 5000 });
          }
        });
      },

      error: e => {
        this.ls.stopLoading();
        this.sb.open(e.error.error, 'Ok', { duration: 5000 });
      }

    });
  }

  clearGene() {
    this.speciesControl.reset('');
    this.gene = undefined;
    this.genesInformation = [];
  }

  private _filter(value: string): Specie[] {
    const filterValue = value.toLowerCase();
    return this.species.filter(option => option.normalizedName.toLowerCase().indexOf(filterValue) >= 0);
  }

  displayFnSpecie(s?: Specie): string | undefined {
    return s ? s.normalizedName : undefined;
  }

  ngOnDestroy() { }
}
