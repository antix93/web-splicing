import { SocketService } from './../../services/socket.service';
import { GeneService } from './../../services/gene.service';
import { Component, OnInit, Input } from '@angular/core';
import { Gene } from 'src/app/models/gene';
import { EnsemblService } from 'src/app/services/ensembl.service';
import { Sequence } from 'src/app/models/sequence';
import { LoadingScreenService } from 'src/app/services/loading-screen';
import { MatSnackBar } from '@angular/material';
import { Router } from '@angular/router';

@Component({
  selector: 'app-gene-information',
  templateUrl: './gene-information.component.html',
  styleUrls: ['./gene-information.component.css']
})
export class GeneInformationComponent implements OnInit {
  @Input() gene: Gene;

  sequence: Sequence;

  constructor(
    private es: EnsemblService,
    private ls: LoadingScreenService,
    private gs: GeneService,
    public ss: SocketService,
    private sb: MatSnackBar,
    private r: Router
  ) { }

  ngOnInit() {
  }

  getTextSequence() {
    this.ls.startLoading();
    this.es.getSequence(this.gene.id).subscribe(f => {
      this.sequence = f;
      this.ls.stopLoading();
    });
  }

  getFasta() {
    this.ss.invokePintron(this.sequence.seq, this.gene);
  }

  getTranscript(chart = false) {
    this.gs.getPintronTranscript(this.gene.id).subscribe(t => {
      if (t) {
        if (chart) {
          this.r.navigate([`/transcriptomic-graph/${this.gene.id}`]);
        } else {
          this.sequence = {};
          this.sequence.seq = t.pintron_output;
        }
      } else {
        this.sb.open('The gene must be processed', 'Ok', { duration: 5000 });
      }
    });
  }

}
