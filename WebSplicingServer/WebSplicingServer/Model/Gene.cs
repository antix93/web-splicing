﻿namespace WebSplicingServer.Model
{
    public class Gene
    {
        public string Id { get; set; }
        public string Object_type { get; set; }
        public string Source { get; set; }
        public int Version { get; set; }
        public string Seq_region_name { get; set; }
        public string Species { get; set; }
        public string Biotype { get; set; }
        public string Db_type { get; set; }
        public string Assembly_name { get; set; }
        public string Description { get; set; }
        public int Start { get; set; }
        public int Strand { get; set; }
        public string Display_name { get; set; }
        public int End { get; set; }
        public string Logic_name { get; set; }
        public bool Elaboration { get; set; }
        public string Pintron_output { get; set; }
    }
}
