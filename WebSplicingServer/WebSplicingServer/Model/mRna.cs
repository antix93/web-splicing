﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Threading.Tasks;

namespace WebSplicingServer.Model
{
    public class mRna
    {
        [DatabaseGenerated(DatabaseGeneratedOption.Identity)]
        public string Id { get; set; }
        public string EnsemblId { get; set; }
        public string Acc { get; set; }
        public string Nid { get; set; }
        public string Pid { get; set; }
        public string Clone { get; set; }
        public string End { get; set; }
        public string Lid { get; set; }
        public string Seqtype { get; set; }
        public string Trace { get; set; }
        public string mRnaSequence { get; set; }
    }
}
