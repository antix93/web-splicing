﻿using Microsoft.AspNetCore.Builder;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSplicingServer.Model
{
    public class Context : DbContext
    {
        public Context(DbContextOptions<Context> options) : base(options) { }

        public DbSet<Species> Species { get; set; }
        public DbSet<Gene> Genes { get; set; }
        public DbSet<mRna> mRna { get; set; }
        public DbSet<Logs> Logs { get; set; }
    }

    public static class DbInitExtensions
    {
        public static IApplicationBuilder DbInit(this IApplicationBuilder app)
        {
            using (var scope = app.ApplicationServices.CreateScope())
            {
                var context = scope.ServiceProvider.GetService<Context>();
                context.Database.Migrate();
            }

            return app;
        }
    }

}
