﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace WebSplicingServer.Model
{
    public class Species
    {
        public string Id { get; set; }
        public string ScientificName { get; set; }
        public string NormalizedName { get; set; }
        public string CommonName { get; set; }
    }
}
