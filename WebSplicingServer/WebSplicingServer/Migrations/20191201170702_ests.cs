﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebSplicingServer.Migrations
{
    public partial class ests : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "mRna",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    EnsemblId = table.Column<string>(nullable: true),
                    Acc = table.Column<string>(nullable: true),
                    Nid = table.Column<string>(nullable: true),
                    Pid = table.Column<string>(nullable: true),
                    Clone = table.Column<string>(nullable: true),
                    End = table.Column<string>(nullable: true),
                    Lid = table.Column<string>(nullable: true),
                    Seqtype = table.Column<string>(nullable: true),
                    Trace = table.Column<string>(nullable: true),
                    mRnaSequence = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_mRna", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "mRna");
        }
    }
}
