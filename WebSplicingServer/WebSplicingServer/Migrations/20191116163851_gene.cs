﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace WebSplicingServer.Migrations
{
    public partial class gene : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.CreateTable(
                name: "Genes",
                columns: table => new
                {
                    Id = table.Column<string>(nullable: false),
                    Object_type = table.Column<string>(nullable: true),
                    Source = table.Column<string>(nullable: true),
                    Version = table.Column<int>(nullable: false),
                    Seq_region_name = table.Column<string>(nullable: true),
                    Species = table.Column<string>(nullable: true),
                    Biotype = table.Column<string>(nullable: true),
                    Db_type = table.Column<string>(nullable: true),
                    Assembly_name = table.Column<string>(nullable: true),
                    Description = table.Column<string>(nullable: true),
                    Start = table.Column<int>(nullable: false),
                    Strand = table.Column<int>(nullable: false),
                    Display_name = table.Column<string>(nullable: true),
                    End = table.Column<int>(nullable: false),
                    Logic_name = table.Column<string>(nullable: true),
                    Elaboration = table.Column<bool>(nullable: false),
                    Pintron_output = table.Column<string>(nullable: true)
                },
                constraints: table =>
                {
                    table.PrimaryKey("PK_Genes", x => x.Id);
                });
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropTable(
                name: "Genes");
        }
    }
}
