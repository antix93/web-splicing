﻿// <auto-generated />
using System;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Infrastructure;
using Microsoft.EntityFrameworkCore.Migrations;
using Microsoft.EntityFrameworkCore.Storage.ValueConversion;
using WebSplicingServer.Model;

namespace WebSplicingServer.Migrations
{
    [DbContext(typeof(Context))]
    [Migration("20200112100529_logs")]
    partial class logs
    {
        protected override void BuildTargetModel(ModelBuilder modelBuilder)
        {
#pragma warning disable 612, 618
            modelBuilder
                .HasAnnotation("ProductVersion", "3.1.0");

            modelBuilder.Entity("WebSplicingServer.Model.Gene", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("TEXT");

                    b.Property<string>("Assembly_name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Biotype")
                        .HasColumnType("TEXT");

                    b.Property<string>("Db_type")
                        .HasColumnType("TEXT");

                    b.Property<string>("Description")
                        .HasColumnType("TEXT");

                    b.Property<string>("Display_name")
                        .HasColumnType("TEXT");

                    b.Property<bool>("Elaboration")
                        .HasColumnType("INTEGER");

                    b.Property<int>("End")
                        .HasColumnType("INTEGER");

                    b.Property<string>("Logic_name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Object_type")
                        .HasColumnType("TEXT");

                    b.Property<string>("Pintron_output")
                        .HasColumnType("TEXT");

                    b.Property<string>("Seq_region_name")
                        .HasColumnType("TEXT");

                    b.Property<string>("Source")
                        .HasColumnType("TEXT");

                    b.Property<string>("Species")
                        .HasColumnType("TEXT");

                    b.Property<int>("Start")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Strand")
                        .HasColumnType("INTEGER");

                    b.Property<int>("Version")
                        .HasColumnType("INTEGER");

                    b.HasKey("Id");

                    b.ToTable("Genes");
                });

            modelBuilder.Entity("WebSplicingServer.Model.Logs", b =>
                {
                    b.Property<int>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("INTEGER");

                    b.Property<string>("Application")
                        .HasColumnType("TEXT");

                    b.Property<string>("Callsite")
                        .HasColumnType("TEXT");

                    b.Property<string>("Exception")
                        .HasColumnType("TEXT");

                    b.Property<string>("Level")
                        .HasColumnType("TEXT");

                    b.Property<DateTime>("Logged")
                        .HasColumnType("TEXT");

                    b.Property<string>("Logger")
                        .HasColumnType("TEXT");

                    b.Property<string>("Message")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Logs");
                });

            modelBuilder.Entity("WebSplicingServer.Model.Species", b =>
                {
                    b.Property<string>("Id")
                        .HasColumnType("TEXT");

                    b.Property<string>("CommonName")
                        .HasColumnType("TEXT");

                    b.Property<string>("NormalizedName")
                        .HasColumnType("TEXT");

                    b.Property<string>("ScientificName")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("Species");
                });

            modelBuilder.Entity("WebSplicingServer.Model.mRna", b =>
                {
                    b.Property<string>("Id")
                        .ValueGeneratedOnAdd()
                        .HasColumnType("TEXT");

                    b.Property<string>("Acc")
                        .HasColumnType("TEXT");

                    b.Property<string>("Clone")
                        .HasColumnType("TEXT");

                    b.Property<string>("End")
                        .HasColumnType("TEXT");

                    b.Property<string>("EnsemblId")
                        .HasColumnType("TEXT");

                    b.Property<string>("Lid")
                        .HasColumnType("TEXT");

                    b.Property<string>("Nid")
                        .HasColumnType("TEXT");

                    b.Property<string>("Pid")
                        .HasColumnType("TEXT");

                    b.Property<string>("Seqtype")
                        .HasColumnType("TEXT");

                    b.Property<string>("Trace")
                        .HasColumnType("TEXT");

                    b.Property<string>("mRnaSequence")
                        .HasColumnType("TEXT");

                    b.HasKey("Id");

                    b.ToTable("mRna");
                });
#pragma warning restore 612, 618
        }
    }
}
