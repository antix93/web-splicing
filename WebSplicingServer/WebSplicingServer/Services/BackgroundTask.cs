﻿using Microsoft.Extensions.Hosting;
using Microsoft.Extensions.Logging;
using System;
using System.Threading;
using System.Threading.Tasks;

namespace WebSplicingServer.Services
{
    internal class BackgroundTask : IHostedService, IDisposable
    {
        private Timer _timer;
        private readonly PintronService _pintronService;
        private readonly ILogger<BackgroundTask> _logger;

        public BackgroundTask(PintronService pintronService, ILogger<BackgroundTask> logger)
        {
            _pintronService = pintronService;
            _logger = logger;
        }

        public Task StartAsync(CancellationToken cancellationToken)
        {
            _timer = new Timer(DoWork, null, TimeSpan.Zero,
                TimeSpan.FromSeconds(30));

            return Task.CompletedTask;
        }

        private async void DoWork(object state)
        {
            await _pintronService.PintronIsProcessing();
        }

        public Task StopAsync(CancellationToken cancellationToken)
        {
            _timer?.Change(Timeout.Infinite, 0);

            return Task.CompletedTask;
        }

        public void Dispose()
        {
            _timer?.Dispose();
        }
    }

}
