﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using WebSplicingServer.Helper;
using WebSplicingServer.Model;

namespace WebSplicingServer.Services
{
    public class AttachedService
    {
        private string genomicDir, rootPath;
        private readonly IServiceProvider _provider;
        private readonly ILogger<AttachedService> _logger;
        private Context _context;
        public AttachedService(
            IHostingEnvironment hostingEnvironment,
            IServiceProvider serviceProvider,
            ILogger<AttachedService> logger
            )

        {
            _logger = logger;
            _provider = serviceProvider;
#if DEBUG
            genomicDir = hostingEnvironment.ContentRootPath + "/GenomicFile";
            rootPath = hostingEnvironment.ContentRootPath;
#else
            genomicDir = "/home/Pintron/doc";
            rootPath = "/home/Pintron/ests";
#endif
        }

        public async Task<bool> PostFasta(string sequence, Gene gene)
        {
            FileManager fm = new FileManager();

            try
            {
                using (IServiceScope scope = _provider.CreateScope())
                {
                    _context = scope.ServiceProvider.GetRequiredService<Context>();

                    var oldGene = _context.Genes.FirstOrDefault(_g => _g.Id == gene.Id);
                    if (oldGene != null)
                        _context.Remove(oldGene);

                    gene.Elaboration = true;
                    _context.Genes.Add(gene);
                    await _context.SaveChangesAsync();

                    var headerFasta = $@">chr{gene.Version}:{gene.Start}:{gene.End}:{gene.Strand}";

                    using (var streamWriter = new StreamWriter($"{genomicDir}/genomic.txt"))
                    {
                        streamWriter.WriteLine(headerFasta);
                        streamWriter.Write(sequence);
                    }

                    var exstractedMrna = _context.mRna.Any(m => m.EnsemblId == gene.Id);

                    if (!exstractedMrna)
                        await MrnaSequenceAsync(gene);

                    await GenerateEstsFile(gene.Id);

                    fm.CreateShellCommand(genomicDir, gene.Species, gene.Display_name);
#if DEBUG
                    fm.UploadFile(genomicDir);
#else
                    fm.LaunchPintron();
#endif
                    return true;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Errore PostFasta()");
                throw e;
            }

        }

        public async Task MrnaSequenceAsync(Gene gene)
        {
            bool findGene = false;
            bool findBlockEnd = false;
            string line;
            Regex geneRx = new Regex($@"(GENE)\s+({gene.Display_name})\b");
            Regex sequenceRx = new Regex(@"(SEQUENCE)\s+.*");
            Regex accRx = new Regex(@"(ACC=)(\w+)");
            Regex nidRx = new Regex(@"(NID=)(\w+)");
            Regex pidRx = new Regex(@"(PID=)(\w+)");
            Regex cloneRx = new Regex(@"(CLONE=)(\w*[\-\:]*\w*)*");
            Regex endRx = new Regex(@"(END=)(\w+)'");
            Regex lidRx = new Regex(@"(LID=)(\w+)");
            Regex seqtypeRx = new Regex(@"(SEQTYPE=)(\w+)");
            Regex traceRx = new Regex(@"(TRACE=)(\w+)");
            List<string> sequenceCollection = new List<string>();
            List<mRna> mRnas = new List<mRna>();

            HttpClient client = new HttpClient();

            try
            {
                using (StreamReader sr = new StreamReader($"{rootPath}/{gene.Species}.data"))
                {
                    while ((line = sr.ReadLine()) != null && !findBlockEnd)
                    {
                        if (!findGene)
                        {
                            findGene = geneRx.IsMatch(line);
                        }
                        else
                        {
                            if (sequenceRx.IsMatch(line)) sequenceCollection.Add(line);

                            // Quando trovo il doppio slash termino tutto
                            if (line == "//") findBlockEnd = true;
                        }
                    }
                }

                // Devo estrarre tutti gli ACC e recuperare le sequenze da NCBI 
                if (sequenceCollection.Count > 0)
                {
                    foreach (var s in sequenceCollection)
                    {
                        var acc = accRx.Match(s).Value.Replace("ACC=", String.Empty);
                        var nid = nidRx.Match(s).Value.Replace("NID=", String.Empty);
                        var pid = pidRx.Match(s).Value.Replace("PID=", String.Empty);
                        var clone = cloneRx.Match(s).Value.Replace("CLONE=", String.Empty);
                        var end = endRx.Match(s).Value.Replace("END=", String.Empty);
                        var lid = lidRx.Match(s).Value.Replace("LID=", String.Empty);
                        var seqtype = seqtypeRx.Match(s).Value.Replace("SEQTYPE=", String.Empty);
                        var trace = traceRx.Match(s).Value.Replace("TRACE=", String.Empty);

                        var baseUrl = $@"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id={acc}&rettype=fasta";
                        await Task.Delay(240);
                        HttpResponseMessage res = await client.GetAsync(baseUrl);
                        using (HttpContent content = res.Content)
                        {
                            string data = await content.ReadAsStringAsync();

                            mRna mRna = new mRna
                            {
                                EnsemblId = gene.Id,
                                Acc = acc,
                                Nid = nid,
                                Pid = pid,
                                Clone = clone,
                                End = end,
                                Lid = lid,
                                Seqtype = seqtype,
                                Trace = trace,
                                mRnaSequence = data
                            };

                            mRnas.Add(mRna);
                        }
                    }

                    _context.mRna.AddRange(mRnas);
                    await _context.SaveChangesAsync();

                }

            }
            catch (Exception e)
            {
                _logger.LogError(e, "Errore MrnaSequenceAsync()");
                throw e;
            }
        }

        public async Task GenerateEstsFile(string ensemblId)
        {
            try
            {
                var geneEsts = await _context.mRna.Where(ge => ge.EnsemblId == ensemblId).ToListAsync();

                using (StreamWriter outputEsts = new StreamWriter(Path.Combine(genomicDir, "ests.txt")))
                {
                    foreach (var ge in geneEsts)
                    {
                        string informationHeader = String.Empty;

                        if (!String.IsNullOrEmpty(ge.Clone))
                            informationHeader += $"/clone={ge.Clone} ";
                        if (!String.IsNullOrEmpty(ge.End))
                            informationHeader += $"/clone_end={ge.End} ";
                        if (!String.IsNullOrEmpty(ge.Acc))
                            informationHeader += $"/gb={ge.Acc} ";
                        if (!String.IsNullOrEmpty(ge.Nid))
                            informationHeader += $"/gi={ge.Nid}";

                        var splittedSequence = ge.mRnaSequence.Trim().Split("\n");
                        var header = splittedSequence[0];
                        var newHeader = header.Replace(">", ">gnl ");
                        newHeader += $" {informationHeader}";

                        var sequence = splittedSequence.Skip(1).Join("\n");

                        outputEsts.WriteLine(newHeader);
                        outputEsts.WriteLine(sequence);

                    }
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Errore GenerateEstsFile()");
                throw e;
            }
        }

    }
}
