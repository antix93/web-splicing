﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using Renci.SshNet;
using WebSplicingServer.Model;
using WebSplicingServer.Socket;

namespace WebSplicingServer.Services
{
    public class PintronService
    {
        private readonly IServiceProvider _provider;
        private readonly ILogger<PintronService> _logger;
        private Context _context;

        public PintronService(IServiceProvider serviceProvider, ILogger<PintronService> logger)
        {
            _provider = serviceProvider;
            _logger = logger;
        }

        public async Task PintronIsProcessing()
        {
            string idGene;
            string pathPintronJson = @"/home/Pintron/out/pintron-full-output.json";
            string json = String.Empty;
            DirectoryInfo di;

            try
            {

                using (IServiceScope scope = _provider.CreateScope())
                {
                    _context = scope.ServiceProvider.GetRequiredService<Context>();
                    SocketHelper _socketHelper = scope.ServiceProvider.GetRequiredService<SocketHelper>();

                    var elaboratingGene = await _context.Genes.FirstOrDefaultAsync(g => g.Elaboration == true);
                    if (elaboratingGene != null)
                    {
                        idGene = elaboratingGene.Id;
#if DEBUG
                        json = CheckPintronOutput();
#else
                    if(System.IO.File.Exists(pathPintronJson))
                    {
                        json = System.IO.File.ReadAllText(pathPintronJson);
                    }
#endif
                        if (json != String.Empty)
                        {
                            elaboratingGene.Pintron_output = json;
                            elaboratingGene.Elaboration = false;
                            _context.Entry(elaboratingGene).State = EntityState.Modified;
                            await _context.SaveChangesAsync();

                            // Clear Pintron folder (out - doc)
#if DEBUG
                            ClearPintronFile();
#else

                        di = new DirectoryInfo(@"/home/Pintron/out");
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
                        di = new DirectoryInfo(@"/home/Pintron/doc");
                        foreach (FileInfo file in di.GetFiles())
                        {
                            file.Delete();
                        }
#endif

                            // Call client via socket and send pintron output
                            _socketHelper.ElaborationResponse(elaboratingGene);
                        }
                    }

                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Errore PintronIsProcessing()");
                throw e;
            }
        }

        public string CheckPintronOutput()
        {
            string host = @"80.211.85.78";
            string username = "root";
            string password = @"Baluba93.1";
            string pintronJson = String.Empty;

            try
            {
                var methods = new List<AuthenticationMethod>();
                methods.Add(new PasswordAuthenticationMethod(username, password));

                ConnectionInfo con = new ConnectionInfo(host, 22, username, methods.ToArray());

                using (var client = new SshClient(con))
                {
                    client.Connect();

                    if (client.IsConnected)
                    {
                        var fileOutput = client.RunCommand("cd /home/Pintron/out; ls").Result;
                        if (fileOutput.Contains("pintron-full-output.json"))
                            pintronJson = client.RunCommand("cd /home/Pintron/out; cat pintron-full-output.json").Result;
                    }
                    else
                    {
                        Console.WriteLine("Not connected");
                    }

                    client.Disconnect();
                    return pintronJson;
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Errore CheckPintronOutput()");
                throw e;
            }
        }

        public void ClearPintronFile()
        {

            string host = @"80.211.85.78";
            string username = "root";
            string password = @"Baluba93.1";

            try
            {
                var methods = new List<AuthenticationMethod>();
                methods.Add(new PasswordAuthenticationMethod(username, password));

                ConnectionInfo con = new ConnectionInfo(host, 22, username, methods.ToArray());

                using (var client = new SshClient(con))
                {
                    client.Connect();

                    if (client.IsConnected)
                    {
                        client.RunCommand(@"cd /home/Pintron/out; rm *");
                        client.RunCommand(@"cd /home/Pintron/doc; rm *");
                    }
                    else
                    {
                        Console.WriteLine("Not connected");
                    }

                    client.Disconnect();
                }
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Errore ClearPintronFile()");
                throw e;
            }
        }
    }
}