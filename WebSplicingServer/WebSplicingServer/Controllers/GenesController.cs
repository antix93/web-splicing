﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using WebSplicingServer.Model;

namespace WebSplicingServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class GenesController : ControllerBase
    {
        private readonly Context _context;

        public GenesController(Context context)
        {
            _context = context;
        }

        // GET: api/Genes
        [HttpGet]
        public async Task<ActionResult<IEnumerable<Gene>>> GetGenes()
        {
            return await _context.Genes.ToListAsync();
        }

        [HttpGet("ProcessingGene")]
        public async Task<ActionResult<Gene>> ProcessingGene()
        {
            return await _context.Genes.Where(g => g.Elaboration).FirstOrDefaultAsync();
        }

        // GET: api/Genes/5
        [HttpGet("{id}")]
        public async Task<ActionResult<Gene>> GetGene(string id)
        {
            var gene = await _context.Genes.FindAsync(id);

            if (gene == null)
            {
                return null;
            }

            return gene;
        }

        // PUT: api/Genes/5
        [HttpPut("{id}")]
        public async Task<IActionResult> PutGene(string id, Gene gene)
        {
            if (id != gene.Id)
            {
                return BadRequest();
            }

            _context.Entry(gene).State = EntityState.Modified;

            try
            {
                await _context.SaveChangesAsync();
            }
            catch (DbUpdateConcurrencyException)
            {
                if (!GeneExists(id))
                {
                    return NotFound();
                }
                else
                {
                    throw;
                }
            }

            return NoContent();
        }

        // POST: api/Genes
        [HttpPost]
        public async Task<ActionResult<Gene>> PostGene(Gene gene)
        {
            _context.Genes.Add(gene);
            await _context.SaveChangesAsync();

            return CreatedAtAction("GetGene", new { id = gene.Id }, gene);
        }

        // DELETE: api/Genes/5
        [HttpDelete("{id}")]
        public async Task<ActionResult<Gene>> DeleteGene(string id)
        {
            var gene = await _context.Genes.FindAsync(id);
            if (gene == null)
            {
                return NotFound();
            }

            _context.Genes.Remove(gene);
            await _context.SaveChangesAsync();

            return gene;
        }

        private bool GeneExists(string id)
        {
            return _context.Genes.Any(e => e.Id == id);
        }
    }
}
