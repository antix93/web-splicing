﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http;
using System.Text.RegularExpressions;
using System.Threading;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.Internal;
using OfficeOpenXml;
using Renci.SshNet;
using Renci.SshNet.Sftp;
using WebSplicingServer.Helper;
using WebSplicingServer.Model;

namespace WebSplicingServer.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private string path, ppkPath, genomicDir, sequenceMrna;
        private readonly Context _context;
        public ValuesController(IHostingEnvironment hostingEnvironment, Context context)
        {
            path = hostingEnvironment.ContentRootPath + @"/Species.xlsx";
            ppkPath = hostingEnvironment.ContentRootPath + @"/privateKey.ppk";
            sequenceMrna = hostingEnvironment.ContentRootPath + @"/Hs.data";
            genomicDir = hostingEnvironment.ContentRootPath + "/GenomicFile";
            _context = context;
        }

        // POST api/values
        [HttpPost]
        public async Task<ActionResult<Species>> PostAsync()
        {
            List<Species> species = new List<Species>();
            FileInfo file = new FileInfo(path);

            using (ExcelPackage package = new ExcelPackage(file))
            {
                ExcelWorksheet worksheet = package.Workbook.Worksheets[0];
                int rows = worksheet.Dimension.Rows;

                for (int i = 1; i <= rows; i++)
                {
                    string commonName = worksheet.Cells[i, 1].Value.ToString();
                    string normalizedName = worksheet.Cells[i, 2].Value.ToString();

                    species.Add(new Species
                    {
                        CommonName = commonName,
                        ScientificName = normalizedName.Replace(" ", "_"),
                        NormalizedName = normalizedName,
                    });
                }
                _context.Species.AddRange(species);
                await _context.SaveChangesAsync();
            }

            return Ok();
        }

        [HttpPost("PostFile")]
        public async Task UploadFileAsync()
        {
            string host = @"80.211.85.78";
            string username = "root";
            string password = @"Baluba93.1";

            FileManager fm = new FileManager();

            fm.CreateShellCommand(genomicDir, "human", "TP53");
            var files = Directory.GetFiles(genomicDir);

            try
            {
                var methods = new List<AuthenticationMethod>();
                methods.Add(new PasswordAuthenticationMethod(username, password));

                ConnectionInfo con = new ConnectionInfo(host, 22, username, methods.ToArray());

                using (var client = new SftpClient(con))
                {
                    client.Connect();

                    if (client.IsConnected)
                    {
                        client.ChangeDirectory("/home/Pintron/doc");
                        foreach (var f in files)
                        {
                            using (var fileStream = new FileStream(f, FileMode.Open))
                            {
                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                client.UploadFile(fileStream, Path.GetFileName(f));
                            }
                        }

                    }
                    else
                    {
                        Console.WriteLine("Not connected");
                    }

                    client.Disconnect();
                }
            }
            catch (Exception e)
            {

            }
        }

        [HttpPost("SSH")]
        public void LaunchPintron()
        {
            string host = @"80.211.85.78";
            string username = "root";
            string password = @"Baluba93.1";

            try
            {
                var methods = new List<AuthenticationMethod>();
                methods.Add(new PasswordAuthenticationMethod(username, password));

                ConnectionInfo con = new ConnectionInfo(host, 22, username, methods.ToArray());

                using (var client = new SshClient(con))
                {
                    client.Connect();

                    if (client.IsConnected)
                    {
                        // client.RunCommand("cd /home/Pintron/doc; chmod +x cmd.sh; ./cmd.sh");
                        var x = client.RunCommand("cd /home/Pintron/out; ls");
                        var res = x.Result.Split();
                        var t = client.RunCommand("cd /home/Pintron/out; cat pintron-full-output.json").Result;
                    }
                    else
                    {
                        Console.WriteLine("Not connected");
                    }

                    client.Disconnect();
                }
            }
            catch (Exception e)
            {

            }
        }

        [HttpPost("Sftp")]
        public void TestSftp()
        {
            string host = @"80.211.85.78";
            string username = "root";
            string password = @"Baluba93.1";

            try
            {
                PrivateKeyFile keyFile = new PrivateKeyFile(ppkPath);
                var keyFiles = new[] { keyFile };

                var methods = new List<AuthenticationMethod>();
                methods.Add(new PasswordAuthenticationMethod(username, password));
                methods.Add(new PrivateKeyAuthenticationMethod(username, keyFiles));

                ConnectionInfo con = new ConnectionInfo(host, 22, username, methods.ToArray());
                using (var client = new SftpClient(con))
                {
                    client.Connect();
                    // Do what you need with the client !

                    client.Disconnect();
                }

            }
            catch (Exception e)
            {

            }
        }

        [HttpPost("Pintron")]
        public void TestPintron()
        {
            string proxyHost = "149.132.157.125";
            int proxyPort = 32762;
            string proxyUsername = "prj_pinw";
            ProxyTypes proxyTypes = ProxyTypes.None;

            string host = "10.0.3.27";
            int port = 22;
            string username = "prj_pinw";
            string password = "Archer1948";

            try
            {
                var methods = new List<AuthenticationMethod>();
                methods.Add(new NoneAuthenticationMethod(username));

                ConnectionInfo con = new ConnectionInfo(host, port, username, proxyTypes, proxyHost, proxyPort, proxyUsername, password, methods.ToArray());
                // ConnectionInfo conMendel = new ConnectionInfo(proxyHost, proxyPort, proxyUsername, methods.ToArray());
                // ConnectionInfo con = new ConnectionInfo(host, port, username, methods.ToArray());

                using (var client = new SftpClient(con))
                {
                    client.Connect();

                    if (client.IsConnected)
                    {
                        //client.RunCommand("ssh prj_pinw@10.0.3.27").Execute();
                        //var x = client.RunCommand(password).Execute();
                    }
                    else
                    {
                        Console.WriteLine("Not connected");
                    }

                    client.Disconnect();
                }
            }
            catch (Exception e)
            {

            }
        }

        [HttpGet("mrna/{ensemblId}/{geneName}")]
        public async Task MrnaSequenceAsync(string ensemblId, string geneName)
        {
            bool findGene = false;
            bool findBlockEnd = false;
            string line;
            Regex geneRx = new Regex($@"(GENE)\s+({geneName})\b");
            Regex sequenceRx = new Regex(@"(SEQUENCE)\s+.*");
            Regex accRx = new Regex(@"(ACC=)(\w+)");
            Regex nidRx = new Regex(@"(NID=)(\w+)");
            Regex pidRx = new Regex(@"(PID=)(\w+)");
            Regex cloneRx = new Regex(@"(CLONE=)(\w*[\-\:]*\w*)*");
            Regex endRx = new Regex(@"(END=)(\w+)'");
            Regex lidRx = new Regex(@"(LID=)(\w+)");
            Regex seqtypeRx = new Regex(@"(SEQTYPE=)(\w+)");
            Regex traceRx = new Regex(@"(TRACE=)(\w+)");
            List<string> sequenceCollection = new List<string>();
            List<mRna> mRnas = new List<mRna>();

            HttpClient client = new HttpClient();

            try
            {
                using (StreamReader sr = new StreamReader(sequenceMrna))
                {
                    while ((line = sr.ReadLine()) != null && !findBlockEnd)
                    {
                        if (!findGene)
                        {
                            findGene = geneRx.IsMatch(line);
                        }
                        else
                        {
                            if (sequenceRx.IsMatch(line)) sequenceCollection.Add(line);

                            // Quando trovo il doppio slash termino tutto
                            if (line == "//") findBlockEnd = true;
                        }
                    }
                }

                // Devo estrarre tutti gli ACC e recuperare le sequenze da NCBI 
                if (sequenceCollection.Count > 0)
                {
                    foreach (var s in sequenceCollection)
                    {
                        var acc = accRx.Match(s).Value.Replace("ACC=", String.Empty);
                        var nid = nidRx.Match(s).Value.Replace("NID=", String.Empty);
                        var pid = pidRx.Match(s).Value.Replace("PID=", String.Empty);
                        var clone = cloneRx.Match(s).Value.Replace("CLONE=", String.Empty);
                        var end = endRx.Match(s).Value.Replace("END=", String.Empty);
                        var lid = lidRx.Match(s).Value.Replace("LID=", String.Empty);
                        var seqtype = seqtypeRx.Match(s).Value.Replace("SEQTYPE=", String.Empty);
                        var trace = traceRx.Match(s).Value.Replace("TRACE=", String.Empty);

                        var baseUrl = $@"https://eutils.ncbi.nlm.nih.gov/entrez/eutils/efetch.fcgi?db=nucleotide&id={acc}&rettype=fasta";
                        await Task.Delay(200);
                        HttpResponseMessage res = await client.GetAsync(baseUrl);
                        using (HttpContent content = res.Content)
                        {
                            string data = await content.ReadAsStringAsync();

                            mRna mRna = new mRna
                            {
                                EnsemblId = ensemblId,
                                Acc = acc,
                                Nid = nid,
                                Pid = pid,
                                Clone = clone,
                                End = end,
                                Lid = lid,
                                Seqtype = seqtype,
                                Trace = trace,
                                mRnaSequence = data
                            };

                            mRnas.Add(mRna);
                        }
                    }

                    _context.mRna.AddRange(mRnas);
                    await _context.SaveChangesAsync();

                }

            }
            catch (Exception e)
            {
                throw;
            }
        }

        [HttpGet("ests/{ensemblId}")]
        public async Task GenerateEstsFile(string ensemblId)
        {
            try
            {
                var geneEsts = await _context.mRna.Where(ge => ge.EnsemblId == ensemblId).ToListAsync();

                using (StreamWriter outputEsts = new StreamWriter(Path.Combine(genomicDir, "ests.txt")))
                {
                    foreach (var ge in geneEsts)
                    {
                        string informationHeader = String.Empty;

                        if (!String.IsNullOrEmpty(ge.Clone))
                            informationHeader += $"/clone={ge.Clone} ";
                        if (!String.IsNullOrEmpty(ge.End))
                            informationHeader += $"/clone_end={ge.End} ";
                        if (!String.IsNullOrEmpty(ge.Acc))
                            informationHeader += $"/gb={ge.Acc} ";
                        if (!String.IsNullOrEmpty(ge.Nid))
                            informationHeader += $"/gi={ge.Nid}";

                        var splittedSequence = ge.mRnaSequence.Trim().Split("\n");
                        var header = splittedSequence[0];
                        var newHeader = header.Replace(">", ">gnl ");
                        newHeader += $" {informationHeader}";

                        var sequence = splittedSequence.Skip(1).Join("\n");

                        outputEsts.WriteLine(newHeader);
                        outputEsts.WriteLine(sequence);

                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }

    }
}
