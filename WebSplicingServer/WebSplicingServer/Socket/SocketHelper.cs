﻿using Microsoft.AspNetCore.SignalR;
using Microsoft.Extensions.Logging;
using System;
using System.Threading.Tasks;
using WebSplicingServer.Model;
using WebSplicingServer.Services;

namespace WebSplicingServer.Socket
{
    public class SocketHelper: Hub
    {
        private readonly AttachedService _attachedService;
        private readonly ILogger<SocketHelper> _logger;

        public SocketHelper(AttachedService attachedController, ILogger<SocketHelper> logger) {
            _attachedService = attachedController;
            _logger = logger;
        }

        public void ElaborationResponse(Gene gene)
        {
            Clients.All.SendAsync("ElaborationResponse", gene);
        }

        public void StartingElaboration(bool isStarted = true)
        {
            Clients.All.SendAsync("StartingElaboration", isStarted);
        }

        public async Task ElaborationForPintron(string sequence, Gene g)
        {
            try
            {
                StartingElaboration();
                await _attachedService.PostFasta(sequence, g);
            }
            catch (Exception e)
            {
                _logger.LogError(e, "Errore ElaborationForPintron()");
                throw e;
            }
        }
    }
}
