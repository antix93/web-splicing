﻿using Microsoft.AspNetCore.Hosting;
using Renci.SshNet;
using System;
using System.Collections.Generic;
using System.IO;

namespace WebSplicingServer.Helper
{
    public class FileManager
    {

        public void UploadFile (string dir)
        {
            string host = @"80.211.85.78";
            string username = "root";
            string password = @"Baluba93.1";

            var files = Directory.GetFiles(dir);

            try
            {
                var methods = new List<AuthenticationMethod>();
                methods.Add(new PasswordAuthenticationMethod(username, password));

                ConnectionInfo con = new ConnectionInfo(host, 22, username, methods.ToArray());

                using (var client = new SftpClient(con))
                {
                    client.Connect();

                    if (client.IsConnected)
                    {
                        client.ChangeDirectory("/home/Pintron/doc");
                        foreach (var f in files)
                        {
                            using (var fileStream = new FileStream(f, FileMode.Open))
                            {
                                client.BufferSize = 4 * 1024; // bypass Payload error large files
                                client.UploadFile(fileStream, Path.GetFileName(f));
                            }
                        }

                    }
                    else
                    {
                        Console.WriteLine("Not connected");
                    }

                    client.Disconnect();

                    LaunchPintron();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void LaunchPintron()
        {
            string host = @"80.211.85.78";
            string username = "root";
            string password = @"Baluba93.1";

            try
            {
                var methods = new List<AuthenticationMethod>();
                methods.Add(new PasswordAuthenticationMethod(username, password));

                ConnectionInfo con = new ConnectionInfo(host, 22, username, methods.ToArray());

                using (var client = new SshClient(con))
                {
                    client.Connect();

                    if (client.IsConnected)
                    {
                        client.RunCommand("cd /home/Pintron/doc; chmod +x cmd.sh; ./cmd.sh");
                    }
                    else
                    {
                        Console.WriteLine("Not connected");
                    }

                    client.Disconnect();
                }
            }
            catch (Exception e)
            {

            }
        }

        public void CreateShellCommand(string directory, string organism, string gene)
        {
            string path = directory + "/cmd.sh";
            string command;
            try
            {
                using(FileStream fs = File.Create(path))
                {
                    using (StreamWriter writer = new StreamWriter(fs))
                    {
                        command = "/home/Pintron/bin/pintron " +
                            "--bin-dir=/home/Pintron/bin " +
                            "--genomic=/home/Pintron/doc/genomic.txt " +
                            "--EST=/home/Pintron/doc/ests.txt " +
                            $"--organism={organism} " +
                            $"--gene={gene}  " +
                            "--output=/home/Pintron/out/pintron-full-output.json " +
                            "--gtf=/home/Pintron/out/pintron-cds-annotated-isoforms.gtf " +
                            "--logfile=/home/Pintron/out/pintron-pipeline-log.txt  " +
                            "--general-logfile=/home/Pintron/out/pintron-log.txt";

                        writer.Write(command);

                    }
                }
            }
            catch (Exception e)
            {
                throw;
            }
        }
    }
}
